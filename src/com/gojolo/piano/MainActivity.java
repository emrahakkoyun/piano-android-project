package com.gojolo.piano;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
public class MainActivity extends Activity {
static	MediaPlayer mp1,mp2,mp3,mp4,mp5,mp6,mp7,mp8,mp9,mp10,mp11,mp12,mp13,mp14,mp15,mp16;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
	    AdView adView = (AdView) this.findViewById(R.id.adView);
	    AdRequest adRequest = new AdRequest.Builder().build();
	    adView.loadAd(adRequest); //adView i yüklüyoruz
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		final  MediaPlayer ritims1= MediaPlayer.create(this,R.raw.ritim1);
		final  MediaPlayer ritims2= MediaPlayer.create(this,R.raw.ritim2);
		final  MediaPlayer ritims3= MediaPlayer.create(this,R.raw.ritim3);
		final  MediaPlayer ritims4= MediaPlayer.create(this,R.raw.ritim4);
     final  Button btn1 = (Button)findViewById(R.id.btn1);
    final   Button btn2 = (Button)findViewById(R.id.btn2);
     final  Button btn3 = (Button)findViewById(R.id.btn3);
     final  Button btn4 = (Button)findViewById(R.id.btn4);
     final  Button ritim1 = (Button)findViewById(R.id.ritim1);
     final  Button ritim2 = (Button)findViewById(R.id.ritim2);
     final  Button ritim3 = (Button)findViewById(R.id.ritim3);
     final  Button ritim4 = (Button)findViewById(R.id.ritim4);
     final TextView text = (TextView)findViewById(R.id.texto);
	final	ImageButton img1 = (ImageButton)findViewById(R.id.img1);
	final	ImageButton img2 = (ImageButton)findViewById(R.id.img2);
	final	ImageButton img3 = (ImageButton)findViewById(R.id.img3);
	final	ImageButton img4 = (ImageButton)findViewById(R.id.img4);
	final	ImageButton img5 = (ImageButton)findViewById(R.id.img5);
	final	ImageButton img6 = (ImageButton)findViewById(R.id.img6);
	final 	ImageButton img7 = (ImageButton)findViewById(R.id.img7);
	final	ImageButton img8 = (ImageButton)findViewById(R.id.img8);
	final	ImageButton img9 = (ImageButton)findViewById(R.id.img9);
	final	ImageButton img10 = (ImageButton)findViewById(R.id.img10);
	final	ImageButton img11 = (ImageButton)findViewById(R.id.img11);
	final	ImageButton img12 = (ImageButton)findViewById(R.id.img12);
	final	ImageButton img13 = (ImageButton)findViewById(R.id.img13);
	final	ImageButton img14 = (ImageButton)findViewById(R.id.img14);
	final	ImageButton img15 = (ImageButton)findViewById(R.id.img15);
	final	ImageButton img16 = (ImageButton)findViewById(R.id.img16);
		  img11.setBackgroundResource(R.drawable.pia3);
		  img12.setBackgroundResource(R.drawable.pia3);
		  img13.setBackgroundResource(R.drawable.pia3);
		  img14.setBackgroundResource(R.drawable.pia3);
		  img15.setBackgroundResource(R.drawable.pia3);
		  img16.setBackgroundResource(R.drawable.pia3);
        	nerde(1);
ritim1.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		if(ritims1.isPlaying())
    	{		
        	ritims1.pause();
        	ritim1.setBackgroundColor(Color.parseColor("#8e8e8e"));	
        	text.setText("");
    	}
		else{ 	ritims1.start();
    	text.setText("Dj Onur...");
    	ritim1.setBackgroundColor(Color.parseColor("#ffffff"));
	}}
});
ritim2.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		if(ritims2.isPlaying())
    	{		
        	ritims2.pause();
        	text.setText("");
        	ritim2.setBackgroundColor(Color.parseColor("#8e8e8e"));	
    	}
		else{ 	ritims2.start();
    	text.setText("Six City ...");
    	ritim2.setBackgroundColor(Color.parseColor("#ffffff"));
	}}
});
ritim3.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		if(ritims3.isPlaying())
    	{		
        	ritims3.pause();
        	text.setText("");
        	ritim3.setBackgroundColor(Color.parseColor("#8e8e8e"));	
    	}
		else{ 	ritims3.start();
    	text.setText("Bartin ...");
    	ritim3.setBackgroundColor(Color.parseColor("#ffffff"));
	}}
});
ritim4.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		if(ritims4.isPlaying())
    	{		
        	ritims4.pause();
        	text.setText("");
        	ritim4.setBackgroundColor(Color.parseColor("#8e8e8e"));	
    	}
		else{ 	ritims4.start();
    	text.setText("Asena ... ...");
    	ritim4.setBackgroundColor(Color.parseColor("#ffffff"));
	}}
});
			btn1.setOnTouchListener(new OnTouchListener() {

	            @Override
	            public boolean onTouch(View v, MotionEvent event) {
	                    switch (event.getAction())
	                    {
	                        case MotionEvent.ACTION_DOWN:{
	                        	btn1.setBackgroundColor(Color.parseColor("#8e8e8e"));
	                        	btn2.setBackgroundColor(Color.parseColor("#131313"));
	                        	btn3.setBackgroundColor(Color.parseColor("#131313"));
	                        	btn4.setBackgroundColor(Color.parseColor("#131313"));

	                            break;}
	                        case MotionEvent.ACTION_UP:
	                        case MotionEvent.ACTION_CANCEL:{
	                        	nerde(1);
	                            break;}
	                    }
	            return false;
	            }
	        });
			btn2.setOnTouchListener(new OnTouchListener() {

	            @Override
	            public boolean onTouch(View v, MotionEvent event) {
	                    switch (event.getAction())
	                    {
	                        case MotionEvent.ACTION_DOWN:{
	                        	btn1.setBackgroundColor(Color.parseColor("#131313"));
	                        	btn2.setBackgroundColor(Color.parseColor("#8e8e8e"));
	                        	btn3.setBackgroundColor(Color.parseColor("#131313"));
	                        	btn4.setBackgroundColor(Color.parseColor("#131313"));
	                            break;}
	                        case MotionEvent.ACTION_UP:
	                        case MotionEvent.ACTION_CANCEL:{
	                        	nerde(2);
	                            break;}
	                    }
	            return false;
	            }
	        });
			btn3.setOnTouchListener(new OnTouchListener() {

	            @Override
	            public boolean onTouch(View v, MotionEvent event) {
	                    switch (event.getAction())
	                    {
	                        case MotionEvent.ACTION_DOWN:{
	                        	btn1.setBackgroundColor(Color.parseColor("#131313"));
	                        	btn2.setBackgroundColor(Color.parseColor("#131313"));
	                        	btn3.setBackgroundColor(Color.parseColor("#8e8e8e"));
	                        	btn4.setBackgroundColor(Color.parseColor("#131313"));
	                            break;}
	                        case MotionEvent.ACTION_UP:
	                        case MotionEvent.ACTION_CANCEL:{
	                        	nerde(3);
	                            break;}
	                    }
	            return false;
	            }
	        });
			btn4.setOnTouchListener(new OnTouchListener() {

	            @Override
	            public boolean onTouch(View v, MotionEvent event) {
	                    switch (event.getAction())
	                    {
	                        case MotionEvent.ACTION_DOWN:{
	                        	btn1.setBackgroundColor(Color.parseColor("#131313"));
	                        	btn2.setBackgroundColor(Color.parseColor("#131313"));
	                        	btn3.setBackgroundColor(Color.parseColor("#131313"));
	                        	btn4.setBackgroundColor(Color.parseColor("#8e8e8e"));
	                            break;}
	                        case MotionEvent.ACTION_UP:
	                        case MotionEvent.ACTION_CANCEL:{
	                        	nerde(4);
	                            break;}
	                    }
	            return false;
	            }
	        });
		img1.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:{
                        	if(mp1.isPlaying())
                        	{	
                        	mp1.pause();
                        	mp1.seekTo(0);
                        	mp1.start();}
                        	else{ 	mp1.start(); }
                            img1.setBackgroundResource(R.drawable.pia2);
                            break;}
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:{
                            img1.setBackgroundResource(R.drawable.pia1);
                            break;}
                    }
            return false;
            }
        });
		img2.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:{
                        	if(mp2.isPlaying())
                        	{	mp2.pause();
                        	mp2.seekTo(0);
                        	mp2.start();}
                        	else{ 	mp2.start(); }
                            img2.setBackgroundResource(R.drawable.pia2);
                            break;}
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:{
                            img2.setBackgroundResource(R.drawable.pia1);
                            break;}
                    }
            return false;
            }
        });
		img3.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:{
                        	if(mp3.isPlaying())
                        	{	mp3.pause();
                        	mp3.seekTo(0);
                        	mp3.start();}
                        	else{ 	mp3.start(); }
                            img3.setBackgroundResource(R.drawable.pia2);
                            break;}
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:{
                            img3.setBackgroundResource(R.drawable.pia1);
                            break;}
                    }
            return false;
            }
        });
		img4.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:{
                        	if(mp4.isPlaying())
                        	{	mp4.pause();
                        	mp4.seekTo(0);
                        	mp4.start();}
                        	else{ 	mp4.start(); }
                            img4.setBackgroundResource(R.drawable.pia2);
                            break;}
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:{
                            img4.setBackgroundResource(R.drawable.pia1);
                            break;}
                    }
            return false;
            }
        });
		img5.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:{
                        	if(mp5.isPlaying())
                        	{	mp1.pause();
                        	mp5.seekTo(0);
                        	mp5.start();}
                        	else{ 	mp5.start(); }
                            img5.setBackgroundResource(R.drawable.pia2);
                            break;}
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:{
                            img5.setBackgroundResource(R.drawable.pia1);
                            break;}
                    }
            return false;
            }
        });
		img6.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:{
                        	if(mp6.isPlaying())
                        	{	mp6.pause();
                        	mp6.seekTo(0);
                        	mp6.start();}
                        	else{ 	mp6.start(); }
                            img6.setBackgroundResource(R.drawable.pia2);
                            break;}
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:{
                            img6.setBackgroundResource(R.drawable.pia1);
                            break;}
                    }
            return false;
            }
        });
		img7.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:{
                        	if(mp7.isPlaying())
                        	{	mp7.pause();
                        	mp7.seekTo(0);
                        	mp7.start();}
                        	else{ 	mp7.start(); }
                            img7.setBackgroundResource(R.drawable.pia2);
                            break;}
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:{
                            img7.setBackgroundResource(R.drawable.pia1);
                            break;}
                    }
            return false;
            }
        });
		img8.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:{
                        	if(mp8.isPlaying())
                        	{	mp8.pause();
                        	mp8.seekTo(0);
                        	mp8.start();}
                        	else{ 	mp8.start(); }
                            img8.setBackgroundResource(R.drawable.pia2);
                            break;}
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:{
                            img8.setBackgroundResource(R.drawable.pia1);
                            break;}
                    }
            return false;
            }
        });
		img9.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:{
                        	if(mp9.isPlaying())
                        	{	mp9.pause();
                        	mp9.seekTo(0);
                        	mp9.start();}
                        	else{ 	mp9.start(); }
                            img9.setBackgroundResource(R.drawable.pia2);
                            break;}
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:{
                            img9.setBackgroundResource(R.drawable.pia1);
                            break;}
                    }
            return false;
            }
        });
		img10.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:{
                        	if(mp10.isPlaying())
                        	{	mp10.pause();
                        	mp10.seekTo(0);
                        	mp10.start();}
                        	else{ 	mp10.start(); }
                            img10.setBackgroundResource(R.drawable.pia2);
                            break;}
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:{
                            img10.setBackgroundResource(R.drawable.pia1);
                            break;}
                    }
            return false;
            }
        });
		img11.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:{
                        	if(mp11.isPlaying())
                        	{	mp11.pause();
                        	mp11.seekTo(0);
                        	mp11.start();}
                        	else{ 	mp11.start(); }
                            img11.setBackgroundResource(R.drawable.pia4);
                            break;}
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:{
                            img11.setBackgroundResource(R.drawable.pia3);
                            break;}
                    }
            return false;
            }
        });
		img12.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:{
                        	if(mp12.isPlaying())
                        	{	mp12.pause();
                        	mp12.seekTo(0);
                        	mp12.start();}
                        	else{ 	mp12.start(); }
                            img12.setBackgroundResource(R.drawable.pia4);
                            break;}
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:{
                            img12.setBackgroundResource(R.drawable.pia3);
                            break;}
                    }
            return false;
            }
        });
		img13.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:{
                        	if(mp13.isPlaying())
                        	{	mp13.pause();
                        	mp13.seekTo(0);
                        	mp13.start();}
                        	else{ 	mp13.start(); }
                            img13.setBackgroundResource(R.drawable.pia4);
                            break;}
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:{
                            img13.setBackgroundResource(R.drawable.pia3);
                            break;}
                    }
            return false;
            }
        });
		img14.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:{
                        	if(mp14.isPlaying())
                        	{	mp14.pause();
                        	mp14.seekTo(0);
                        	mp14.start();}
                        	else{ 	mp14.start(); }
                            img14.setBackgroundResource(R.drawable.pia4);
                            break;}
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:{
                            img14.setBackgroundResource(R.drawable.pia3);
                            break;}
                    }
            return false;
            }
        });
		img15.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:{
                        	if(mp15.isPlaying())
                        	{	mp15.pause();
                        	mp15.seekTo(0);
                        	mp15.start();}
                        	else{ 	mp15.start(); }
                            img15.setBackgroundResource(R.drawable.pia4);
                            break;}
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:{
                            img15.setBackgroundResource(R.drawable.pia3);
                            break;}
                    }
            return false;
            }
        });
		img16.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:{
                        	if(mp16.isPlaying())
                        	{	mp16.pause();
                        	mp16.seekTo(0);
                        	mp16.start();}
                        	else{ 	mp16.start(); }
                            img16.setBackgroundResource(R.drawable.pia4);
                            break;}
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:{
                            img16.setBackgroundResource(R.drawable.pia3);
                            break;}
                    }
            return false;
            }
        });
		
	}
	public int nerde(int i)
	{
		switch(i){
		case 1:{
	    mp1 = MediaPlayer.create(this,R.raw.a1);
	    mp2 = MediaPlayer.create(this,R.raw.a2);
	    mp3 = MediaPlayer.create(this,R.raw.a3);
		mp4 = MediaPlayer.create(this,R.raw.a4);
	    mp5 = MediaPlayer.create(this,R.raw.a5);
		mp6 = MediaPlayer.create(this,R.raw.a6);
		mp7 = MediaPlayer.create(this,R.raw.a7);
		mp8 = MediaPlayer.create(this,R.raw.a8);
		mp9 = MediaPlayer.create(this,R.raw.a9);
		mp10 = MediaPlayer.create(this,R.raw.a10);
		mp11 = MediaPlayer.create(this,R.raw.a11);
		mp12 = MediaPlayer.create(this,R.raw.a12);
		mp13 = MediaPlayer.create(this,R.raw.a13);
		mp14 = MediaPlayer.create(this,R.raw.a14);
		mp15 = MediaPlayer.create(this,R.raw.a15);
		mp16 = MediaPlayer.create(this,R.raw.a16);
		break;
		}
		case 2:{
		    mp1 = MediaPlayer.create(this,R.raw.b1);
		    mp2 = MediaPlayer.create(this,R.raw.b2);
		    mp3 = MediaPlayer.create(this,R.raw.b3);
			mp4 = MediaPlayer.create(this,R.raw.b4);
		    mp5 = MediaPlayer.create(this,R.raw.b5);
			mp6 = MediaPlayer.create(this,R.raw.b6);
			mp7 = MediaPlayer.create(this,R.raw.b7);
			mp8 = MediaPlayer.create(this,R.raw.b8);
			mp9 = MediaPlayer.create(this,R.raw.b9);
			mp10 = MediaPlayer.create(this,R.raw.b10);
			mp11 = MediaPlayer.create(this,R.raw.b11);
			mp12 = MediaPlayer.create(this,R.raw.b12);
			mp13 = MediaPlayer.create(this,R.raw.b13);
			mp14 = MediaPlayer.create(this,R.raw.b14);
			mp15 = MediaPlayer.create(this,R.raw.b15);
			mp16 = MediaPlayer.create(this,R.raw.b16);
			break;
		}
		case 3:{
		    mp1 = MediaPlayer.create(this,R.raw.c1);
		    mp2 = MediaPlayer.create(this,R.raw.c2);
		    mp3 = MediaPlayer.create(this,R.raw.c3);
			mp4 = MediaPlayer.create(this,R.raw.c4);
		    mp5 = MediaPlayer.create(this,R.raw.c5);
			mp6 = MediaPlayer.create(this,R.raw.c6);
			mp7 = MediaPlayer.create(this,R.raw.c7);
			mp8 = MediaPlayer.create(this,R.raw.c8);
			mp9 = MediaPlayer.create(this,R.raw.c9);
			mp10 = MediaPlayer.create(this,R.raw.c10);
			mp11 = MediaPlayer.create(this,R.raw.c11);
			mp12 = MediaPlayer.create(this,R.raw.c12);
			mp13 = MediaPlayer.create(this,R.raw.c13);
			mp14 = MediaPlayer.create(this,R.raw.c14);
			mp15 = MediaPlayer.create(this,R.raw.c15);
			mp16 = MediaPlayer.create(this,R.raw.c16);
			break;
		}
		case 4:{
		    mp1 = MediaPlayer.create(this,R.raw.d1);
		    mp2 = MediaPlayer.create(this,R.raw.d2);
		    mp3 = MediaPlayer.create(this,R.raw.d3);
			mp4 = MediaPlayer.create(this,R.raw.d4);
		    mp5 = MediaPlayer.create(this,R.raw.d5);
			mp6 = MediaPlayer.create(this,R.raw.d6);
			mp7 = MediaPlayer.create(this,R.raw.d7);
			mp8 = MediaPlayer.create(this,R.raw.d8);
			mp9 = MediaPlayer.create(this,R.raw.d9);
			mp10 = MediaPlayer.create(this,R.raw.d10);
			mp11 = MediaPlayer.create(this,R.raw.d11);
			mp12 = MediaPlayer.create(this,R.raw.d12);
			mp13 = MediaPlayer.create(this,R.raw.d13);
			mp14 = MediaPlayer.create(this,R.raw.d14);
			mp15 = MediaPlayer.create(this,R.raw.d15);
			mp16 = MediaPlayer.create(this,R.raw.d16);
			break;
		}
		}
		return i;
		
	}
	
	
}
